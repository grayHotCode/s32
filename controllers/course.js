const Course = require("../models/Course");

const auth = require("../auth");


module.exports.addCourse = (reqBody) =>{


	let newCourse = new Course ({
		name: reqBody.name, 
		description: reqBody.description,
		price: reqBody.price 
		
	})

	return newCourse.save().then((course, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})



}


// Retrieve All the courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
}


// Retrieve all Active course
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	});
}


// Retrieve specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
}

// Retrieve specific course and details
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		
		let course = {
			name: result.name,
			description: result.description
		}

		return course;
	});
}



// Update a course
module.exports.updateCourse = (reqParams, reqBody) =>{

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	// Syntax:
		// findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) =>{
		if(error){
			return false
		}else{
			return true
		}

	});
}


module.exports.updateCourse = (reqParams, reqBody) =>{

	let updatedCourse = {
		isActive: reqBody.isActive
	}

	// Syntax:
		// findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) =>{
		if(error){
			return false
		}else{
			return true
		}

	});
}